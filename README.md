***Wallet***


Una Wallet es un programa que almacena  criptodivisas y para nuestro caso, generará la llave pública y privada necesarias para continuar con el consumo de los servicios:

***Consultar fondos*** y ***registrar Transacción.***

Para la generación de la llave privada, será necesario proporcionar 10 palabras y el correo electrónico, la fecha-hora seran tomadas por el sistemas, los cuales serán registrados en un archivo *.txt y al continuar con cualquiera de los siguientes servicios, se visualizarán para su agilidad en la transacción; el archivo no será almacenable.

***Consultar fondos:***
		Para este servicio será necesario proporcionar  la llave privada y  posterior a ello, retornará el saldo respectivo.

***Realizar Transacción:***
		  Este servicio requiere de las llaves públicas de quien realiza la transacción, a quien se le va a enviar el dinero y finalmente el monto a transferir deberá retornará la información respectiva de acuerdo a la lógica del componente del Blockchain.



<p align="center">
![Componente](walletComponent.jpg)
</p>
                                                                        



El desarrollo del presente proyecto está bajo los lenguajes de PHP, javascript, css y html.


Wallet se compone de los siguientes archivos: 

***1.	css***

En esta clase se colocaron los estilos para la visualización de Wallet


table {
    color: black;   
    width: 100%;
}
td {
    border: black 1px solid;
}

td.dato {
    padding: 0px;
    margin: 0px;
    width: 20%;
    /*Letra*/
    text-align: center;
    background: linear-gradient(to bottom, yellow, orange, red);
    font-weight: bold;
}


input, textarea, select{
    padding: 0px;
    margin: 0px;
    width: 100%;
}

input.boton{
    padding: 5px;
    margin: 0px;
    cursor: pointer;    
    background: linear-gradient(to bottom, gray, white);
    text-align: center;
}

button {
    width: 100%;
    padding: 5px;
    margin: 0px;
    cursor: pointer;    
    background: linear-gradient(to bottom, gray, white);
    text-align: center;
}


input.boton:hover {background: linear-gradient(to bottom, white, gray);}

***2.	HTML***

*a.	Index*

 Es la vista principal donde se encuentras las opciones de Generar Llaves, Registrar Transacción y Consultar fondos. 

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Cliente Wallet</title>   
        <link rel="stylesheet" type="text/css" href="../CSS/wallet.css">
        <script src="../JS/Keys.js" type="text/javascript"></script>
    </head>
    <body onload="privateKey(); publicKey();">
        <div>
            <table>
                <tr>
                    <td class="dato">Llave Privada</td>
                    <td>
                        <input id="privateKey" disabled>
                    </td>
                <tr>
                <tr>
                    <td class="dato">Llave Pública</td>
                    <td>
                      <input id="publicKey" disabled>
                    </td>
                <tr>
            </table>
            <div id="respuesta"></div>
        <div>

            <hr/>
            <h1 align="center">SERVICIOS DISPONIBLES</h1>
            <hr/>
            <ul>
                <li><a href="./walletKeysGenerator.html">Generador Llaves</a></li> 
                <li><a href="./walletTransaction.html">Registrar Transacción</a></li> 
                <li><a href="./walletFounds.html">Consultar Fondos</a></li> 
            </ul>

    </body>
</html>

*b.	walletFounds.html*

En esta vista se consulta los fondos  para este proceso pide el dato de llave pública

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Consultar Fondos</title> 
        <link rel="stylesheet" type="text/css" href="../CSS/wallet.css">
        <script src="../JS/Keys.js" type="text/javascript"></script>
        <script src="../JS/consumoWS.js" type="text/javascript"></script>
    </head>
    <body onload="publicKey();">
        <a href="./index.html">Servicios Disponibles</a>
        <hr>
        <div align="center">
            <h1>CONSULTAR FONDOS</h1>
        </div>
        <hr>
        <div>
            <form method="POST" action="../PHP/walletFounds.php">
                <table>
                    <tr>
                        <td class="dato">Llave Pública</td>
                        <td>
                            <input id="publicKey" name="publicKey" readonly>
                        </td>
                    <tr>
                    <tr>
                        <td class="dato">Fondos</td>
                        <td>
                            <input id="respuesta" readonly>
                        </td>
                    <tr>
                </table>
                <input class="boton" type="submit" value="CONSULTAR FONDOS" onclick="consumirWSFounds()">
            </form>
        </div>
    </body>
</html>


*c.	  WalletKeysGenerator.html*

En esta vista se ingresa los datos de email por parte del usuario y 10 palabras para poder generar la llave privada 

<html>

<head>
    <title>Generador Llaves</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="../CSS/wallet.css">
</head>

<body>
    <a href="./index.html">Servicios Disponibles</a>
    <hr>
    <div align="center">
        <h1>GENERADOR DE LLAVE PRIVADA</h1>
    </div>
    <hr>
    <div>
        <form action="../PHP/walletKeysGenerator.php" method="POST">
            <table>
                <tr>
                    <td class="dato">Email</td>
                    <td width="40%">
                        <input name="usuario" placeholder="Usuario" required>
                    </td>
                    <td width="40%">
                        <select name="dominio" required>
                            <option value="@outlook.com">@outlook.com</option>
                            <option value="@gmail.com">@gmail.com</option>
                            <option value="@yahoo.com">@yahoo.com</option>
                        </select>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td class="dato">Palabra #1</td>
                    <td>
                        <input name="palabraUno" placeholder="Palabra #1" required>
                    </td>
                </tr>
                <tr>
                    <td class="dato">Palabra #2</td>
                    <td>
                        <input name="palabraDos" placeholder="Palabra #2" required>
                    </td>
                </tr>
                <tr>
                    <td class="dato">Palabra #3</td>
                    <td>
                        <input name="palabraTres" placeholder="Palabra #3" required>
                    </td>
                </tr>
                <tr>
                    <td class="dato">Palabra #4</td>
                    <td>
                        <input name="palabraCuatro" placeholder="Palabra #4" required>
                    </td>
                </tr>
                <tr>
                    <td class="dato">Palabra #5</td>
                    <td>
                        <input name="palabraCinco" placeholder="Palabra #5" required>
                    </td>
                </tr>
                <tr>
                    <td class="dato">Palabra #6</td>
                    <td>
                        <input name="palabraSeis" placeholder="Palabra #6" required>
                    </td>
                </tr>
                <tr>
                    <td class="dato">Palabra #7</td>
                    <td>
                        <input name="palabraSiete" placeholder="Palabra #7" required>
                    </td>
                </tr>
                <tr>
                    <td class="dato">Palabra #8</td>
                    <td>
                        <input name="palabraOcho" placeholder="Palabra #8" required>
                    </td>
                </tr>
                <tr>
                    <td class="dato">Palabra #9</td>
                    <td>
                        <input name="palabraNueve" placeholder="Palabra #9" required>
                    </td>
                </tr>
                <tr>
                    <td class="dato">Palabra #10</td>
                    <td>
                        <input name="palabraDiez" placeholder="Palabra #10" required>
                    </td>
                </tr>
            </table>
            <input class="boton" type="submit" value="GENERAR LLAVE">
        </form>
    </div>
</body>

</html>


*d.	walletTransaction.html*

En esta vista se realiza la transacción pidiendo dos datos llave publica1 y llave publica dos, el monto y el estado.


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Registrar Transacción</title>    
        <link rel="stylesheet" type="text/css" href="../CSS/wallet.css">
        <script src="../JS/Keys.js" type="text/javascript"></script>
        <script src="../JS/consumoWS.js" type="text/javascript"></script>
    </head>
    <body onload="publicKey();">
        <a href="./index.html">Servicios Disponibles</a>
        <hr>
        <div align="center">
            <h1>REGISTRAR TRANSACCIÓN</h1>
        </div>
        <hr>
        <div>
            <form method="POST" action="../PHP/walletTransaction.php">
                <table>
                    <tr>
                        <td class="dato">Llave Pública Wallet 1</td>
                        <td>
                            <input id="publicKey" name="publicKey" readonly>
                        </td>
                    <tr>
                    <tr>
                        <td class="dato">Llave Pública Wallet 2</td>
                        <td>
                            <input id="publicKey_toWallet" name="publicKey_toWallet" placeholder="Llave Pública Wallet 2" required>
                        </td>
                    <tr>
                    <tr>
                        <td class="dato">Monto</td>
                        <td>
                            <input id="monto" name="monto" type="number" min="1" placeholder="Monto" required>
                        </td>
                    <tr>
                    <tr>
                        <td class="dato">Respuesta</td>
                        <td>
                            <input id="respuesta" readonly>
                        </td>
                    <tr>
                </table>
                <input class="boton" type="submit" value="REGISTRAR TRANSACCIÓN" onclick="consumirWSTransaction()">
            </form>
        </div>
    </body>
</html>

***3.  consumoWS.js***

*a.	Esa una clase javascript la cual tiene una función de transacción que va a consumir al coordinador por medio de un objJson la cual valida dirección de origen y la llave pública.*


function consumirWSTransaction(){    
    
    //alert("consumirWSRestTransaction()");
    var publicKey = document.getElementById("publicKey").value;
    //alert(publicKey);
    var publicKey_toWallet = document.getElementById("publicKey_toWallet").value;
    //alert(publicKey_toWallet);
    var monto = document.getElementById("monto").value;
    //alert(monto);
    var formatoJSON = '{"dir_origen":"'+publicKey+'","dir_destino":"'+publicKey_toWallet+'","monto":'+monto+'}';
    alert(formatoJSON);
    var objJSON = JSON.parse(formatoJSON);
    alert(objJSON.dir_origen);
    alert(objJSON.dir_destino);
    alert(objJSON.monto);

        var xmlhttp = new XMLHttpRequest();
    
        xmlhttp.onreadystatechange = function(){
        if (this.readyState == 4 && this.status == 200){
            document.getElementById("respuesta").innerHTML = this.responseText;
        }      
        };
        xmlhttp.open("POST", "http://142.44.246.92:8080/coordinator/RestU/rest/coordinator/pasodesaldo", true);
        xhttp.setRequestHeader('Content-Type', 'application/json');
        xmlhttp.send(JSON.stringify(objJSON));
    
    
}

function consumirWSFounds() {
        
    //alert("consumirWSFounds()");
    var publicKey = document.getElementById("publicKey").value;
    //alert(publicKey);
    var formatoJSON = '{"dir_origen":"'+publicKey+'"'+'}';
    alert(formatoJSON);
    var objJSON = JSON.parse(formatoJSON);
    alert(objJSON.dir_origen);


        var xmlhttp = new XMLHttpRequest();
        
        xmlhttp.onreadystatechange = function(){
        if (this.readyState == 4 && this.status == 200){
            document.getElementById("respuesta").innerHTML = this.responseText;
        }      
        };
        xmlhttp.open("POST", "http://142.44.246.92:8080/coordinator/RestU/rest/coordinator/consultarsaldo", true);
        xhttp.setRequestHeader('Content-Type', 'application/json');
        xmlhttp.send(JSON.stringify(objJSON));
    
}

*b.	Keys.js*

En esta clase están las  llaves públicas y privada y se envía a archivo privatekey.php y publickey.php respectivamente 

function privateKey(){    
    //alert("privateKey()");
    var xmlhttp = new XMLHttpRequest();
    
    xmlhttp.onreadystatechange = function(){
    if (this.readyState == 4 && this.status == 200){
        document.getElementById("privateKey").value = this.responseText;
    }      
    };
    xmlhttp.open("GET", "../PHP/privateKey.php", true);
    xmlhttp.send();
 
}

function publicKey(){    
    //alert("publicKey()");
    var xmlhttp = new XMLHttpRequest();
    
    xmlhttp.onreadystatechange = function(){
    if (this.readyState == 4 && this.status == 200){
        document.getElementById("publicKey").value = this.responseText;
    }      
    };
    xmlhttp.open("GET", "../PHP/publicKey.php", true);
    xmlhttp.send();
    
}

***4. Carpeta keys***

La  carpeta contiene los  repositorios de las  las llaves públicas y llave privada en un .TXT donde la  llave pública tiene los datos del correo y los campo 10 campo de los caracteres y la llave privada tiene  el SHA-256 del archivo de la llave pública. 


llavePrivada || llavePublica



**5. Llaves**

*a.	PrivateKey.php*

En esta clase se encuentra la validación que exista el archivo de la clave privada si no se encuentra crea el archivo .txt

<?php

$archivoLlavePrivada = "../KEYS/llavePrivada.txt";
$mensaje;

if (file_exists($archivoLlavePrivada)) {
    $file = fopen("../KEYS/llavePrivada.txt", "r");
    $mensaje = fgets($file);
    fclose($file);
} else {
    $mensaje = "";
}
echo $mensaje;

?>

*b.	PublicKey.php*

En esta clase se encuentra la validación que exista el archivo de la clave publica si no se encuentra crea el archivo .txt

<?php

$archivoLlavePublica = "../KEYS/llavePublica.txt";
$mensaje;

if (file_exists($archivoLlavePublica)) {
    $file = fopen("../KEYS/llavePublica.txt", "r");
    $mensaje = fgets($file);
    fclose($file);
} else {
    $mensaje = "";

}
echo $mensaje;

?>

*c.	WalletFounds.php*


Se valida si existe una llave pública y redirecciona el index a formulario  consulta de fondos

<?php

if ( $_POST['publicKey']!=null ){
    
    $publicKey = $_POST['publicKey'];
        
    $link = '<a href="' . '../HTML/index.html">Servicios Disponibles</a>';
    $mensaje = "Consultar los Fondos de " . $publicKey;
    
    
}else{

    $link = '<a href="' . '../HTML/walletKeysGenerator.html">Generador Llaves</a>';
    $mensaje = "Genere su llave privada para usar este servicio.";

}

echo $link . "<hr>" . $mensaje;

*d. walletKeysGenerator.php*


En este método lista el correo del usuario y las 10 palabra las guarda en el txt llamado llavepublica.txt después los encripta.creando un txt llamada llaveprivada.txt y guarda el sha-256

<?php

$usuario = $_POST["usuario"];
$dominio = $_POST["dominio"];
$palabraUno = $_POST["palabraUno"];
$palabraDos = $_POST["palabraDos"];
$palabraTres = $_POST["palabraTres"];
$palabraCuatro = $_POST["palabraCuatro"];
$palabraCinco = $_POST["palabraCinco"];
$palabraSeis = $_POST["palabraSeis"];
$palabraSiete = $_POST["palabraSiete"];
$palabraOcho = $_POST["palabraOcho"];
$palabraNueve = $_POST["palabraNueve"];
$palabraDiez = $_POST["palabraDiez"];
 
$llavePrivada = $usuario . $dominio . $palabraUno . $palabraDos . $palabraTres . $palabraCuatro . $palabraCinco . $palabraSeis . $palabraSiete . $palabraOcho . $palabraNueve . $palabraDiez . time();
$archivoLlavePrivada = fopen("../KEYS/llavePrivada.txt", "w");
fwrite($archivoLlavePrivada, $llavePrivada);

$llavePublica = hash('sha256', $llavePrivada);
$archivoLlavePublica = fopen("../KEYS/llavePublica.txt", "w");
fwrite($archivoLlavePublica, $llavePublica);

$serviciosDisponibles = '<a href="' . '../HTML/index.html">Servicios Disponibles</a>';
$mensaje = "Tu llave privada es: " . $llavePrivada . "<br>" . "Tu llave pública es: " . $llavePublica;
echo $serviciosDisponibles . "<hr>" . $mensaje;

?>



<?php

if ( $_POST['publicKey']!=null && $_POST['publicKey_toWallet']!=null && $_POST['monto']!=null ){
    
    $publicKey = $_POST['publicKey'];
    $publicKey_toWallet = $_POST['publicKey_toWallet'];
    $monto = $_POST['monto'];
    
    $link = '<a href="' . '../HTML/index.html">Servicios Disponibles</a>';
    $mensaje = "De: " . $publicKey . "<br>" . "Para: " .  $publicKey_toWallet . "<br>" . "Monto: " . $monto;
    
}else{
    $link = '<a href="' . '../HTML/walletKeysGenerator.html">Generador Llaves</a>';
    $mensaje = "Genere su llave privada para usar este servicio.";
}
echo $link . "<hr>" . $mensaje;
?>

*d.	walletTransaction.php*

<?php

if ( $_POST['publicKey']!=null && $_POST['publicKey_toWallet']!=null && $_POST['monto']!=null ){
    
    $publicKey = $_POST['publicKey'];
    $publicKey_toWallet = $_POST['publicKey_toWallet'];
    $monto = $_POST['monto'];
    
    $link = '<a href="' . '../HTML/index.html">Servicios Disponibles</a>';
    $mensaje = "De: " . $publicKey . "<br>" . "Para: " .  $publicKey_toWallet . "<br>" . "Monto: " . $monto;
    
}else{
    $link = '<a href="' . '../HTML/walletKeysGenerator.html">Generador Llaves</a>';
    $mensaje = "Genere su llave privada para usar este servicio.";
}
echo $link . "<hr>" . $mensaje;
?>

